//Domenico Cuscuna 2038364

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class TestCone {
    
    @Test
    public void exceptionTest1(){
        try {
            new Cone(4,-5);
            fail("Should throw an exception.");
        } catch (IllegalArgumentException e) {
            //good!
        }
    }
    @Test
    public void exceptionTest2(){
        try {
            new Cone(0,4);
            fail("Should throw an exception.");
        } catch (IllegalArgumentException e) {
            //good!
        }
    }

    @Test
    public void testGetHeight(){
        Cone testing = new Cone(4,5);
        assertEquals(4, testing.getHeight());
    }

    @Test
    public void testGetRadius(){
        Cone testing = new Cone(4,5);
        assertEquals(5, testing.getRadius());
    }

    @Test
    public void testGetVolume(){
        Cone testing = new Cone(5,6);
        assertEquals(188.49556, testing.getVolume(), 0.0001);
    }

    @Test
    public void testGetSurfaceArea(){
        Cone testing = new Cone(6,7);
        assertEquals(356.68641, testing.getSurfaceArea(), 0.0001);
    }


}
