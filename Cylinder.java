//Domenico Cuscuna 2038364

import java.lang.Math;

public class Cylinder{
    private double height;
    private double radius;

    public Cylinder(double height, double radius){
        if (height <= 0 || radius <= 0){
            throw new IllegalArgumentException("Height and radius must be greater than 0.");
        }
        this.height = height;
        this.radius = radius;
    }

    public double getHeight(){
        return this.height;
    }
    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){

        return (Math.PI * Math.pow(this.radius, 2)) * this.height;
    }

    public double getSurfaceArea(){
        return (Math.PI * 2) * (Math.pow(this.radius, 2)) + (2  * (Math.PI * this.radius * this.height));
    }
}