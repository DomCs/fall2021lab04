// Adam Atamnia 2036819

public class Cone{
    private double height;
    private double radius;

    public Cone(double height, double radius){
        if (height <= 0 || radius <= 0){
            throw new IllegalArgumentException("Height and radius must be greater than 0.");
        }
        this.height = height;
        this.radius = radius;
    }

    public double getHeight(){
        return this.height;
    }
    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        return Math.PI * this.radius*this.radius * (this.height/3);
    }
    public double getSurfaceArea(){
        return Math.PI * this.radius* (this.radius + Math.sqrt(this.radius*this.radius + this.height*this.height));
    }
}