// Adam Atamnia 2036819

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestCylinder {
    @Test
    public void exceptionTest1(){
        try {
            new Cylinder(-1, 2);
            fail("Should throw an exception.");
        } catch (IllegalArgumentException e) {
            //good!
        }
    }
    @Test
    public void exceptionTest2(){
        try {
            new Cylinder(12, 0);
            fail("Should throw an exception.");
        } catch (IllegalArgumentException e) {
            //good!
        }
    }
    @Test
    public void getHeightTest(){
        Cylinder cyl = new Cylinder(4, 2);
        assertEquals(4, cyl.getHeight());
    }
    @Test
    public void getRadiusTest(){
        Cylinder cyl = new Cylinder(4, 2);
        assertEquals(2, cyl.getRadius());
    }
    @Test
    public void getVolumeTest(){
        Cylinder cyl = new Cylinder(4, 2);
        assertEquals(50.26548, cyl.getVolume(), 0.0001);
    }
    @Test
    public void getSurfaceAreaTest(){
        Cylinder cyl = new Cylinder(4, 2);
        assertEquals(75.39822, cyl.getSurfaceArea(), 0.0001);
    }
}
